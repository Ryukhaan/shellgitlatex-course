# Well done student !

It's time for you to learn in pratice what a script is.

## Let's start slowly but surely

### What a script is ?
Copy and paste the followings command into the script (left screen):
Once you have copy-pasted, click on the button **Run**.
```shell
#!/bash

echo "Where am I?"
pwd
echo "Let's create a folder"
mkdir tmp
echo "Ok! Now move into it"
cd tmp
echo "Am I sure to be in the directory?"
pwd
```

What happened on the standard output ? (right screen)

#### NB

Again the first line is a shebang (or hashbang) it tells to your kernel which interperter to use to execute the script.
Normally the shebang looks like **#!/bin/bash**, you will also find it for python **#!/bin/python3**

### Commentary

Try this script and see what it prompts in (standard) output.
(Copy and paste the script on the right screen then click on **Run**)
```shell
#!/bash

# Where am I?
pwd
cd ..
# Ok, what are the files and folder here ?
ls -a
# Mmh, I dunno what is a file and what is a directory. Let's try an option
ls -al
# Better, so .config/ is a directory since its line starts with a 'd'
cd .config/
ls -l
cd pulse/
# Where am I?
pwd
ls
echo "I found a cookie!"
```

#### NB

If you want to make commentary inside your script use the character **#** at the start of the line.

### Variables

So, let's use some variables - no need to copy and paste in the script space you can directly paste in the command-line (left screen):
```shell
# My first variable
a="Hello World!"
echo a
```

Oh! Something went wrong. Let's try this command:
```shell
echo $a
```

That's better :)

#### NB
To declare a variable you have to use *variable_name=what_you_want*
Beware to the space! Indeed don't do this (try it to see what happens):
```shell
a = "Hello World!"
```
or
```shell
a ="Hello World!"
```
or
```shell
a= "Hello World!"
```

As you can see, in shell spaces are really really really important.

### No Quote
So, now let's play with variables (still directly in command-line screen):
```shell
a=42
echo $a
```

OK. Now, let's execute:
```shell
b=bookshelf
echo $b
```

### Simple Quote
What if I use a simple quote ?
```shell
c='date'
echo $c
```

### Double Quote
And double quote ?
```shell
d="A dog with a knife"
echo $d
```

### Combine Simple and double quote
What if I combine quote and double quote?
```shell 
echo "$a"
```

Another combination:
```shell
echo "$d"
```

### More, more, ever more combinations!
Execute the command:
```shell
echo "$c"
```
then,
```shell
echo '$c'
```

What happens ?

#### NB
Remind *c='date'* so *'$c' == ''$c''* which means that the first quotes are empty and the second one is also empty. Thus it will displat *$c*

### Backquote

Let's try these commands:
```shell
c=`date`
echo $c
```

Oh...mindblowing !

#### NB
Indeed, backquote will interpert as a command what is inside (the backquotes).

```shell
echo `$a`
```
It is normal, 42 is not a command...
So, what happens if I do this? Will it prompt again the date? Or something else?
```shell
echo '$c'
```

Just one more thing!
```shell
c=The turtle
```

### Morality

Be very careful with the typographical difference between quote and backquote and avoid confusing them.
To sum up:
1. Double quotes (" ") are open to interpretation (meaning it is possible to execute a command inside);
2. Single quotes  (' ') block interpretation (meaning it is impossible to execute a command inside);
3. Backquote (\` \`) interferes with what's between them (meaning that it execute as a command).

## Your turn !

### Exercise 1

Write a script!

Declare a variable with the path of the Home Directory. Then move to folder by using the variable

<details><summary>Here's the solution</summary>

```shell
#!/bash

VAR=$HOME 
cd $VAR
```

</details>

### Exercise 2

Write a script!

By using variable**s**, prompt the results of the operation 27*538

<details><summary>Here's the solution</summary>

```shell
#!/bash
 
let A=27 
let B=538
let C=$A*$B
echo $C
```

</details>

### Exercise 3

Write a script!

Declare a variable with the manual of the manual (reminder the manual is the command **man**), then display its content.

<details><summary>Here's the solution</summary>

```shell
#!/bash
  
MANUAL=`man man`
echo $MANUAL
```

</details>
