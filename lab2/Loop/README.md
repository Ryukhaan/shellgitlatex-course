# Loop and Conditionnal

In this second part, let's do something a little more "simple" since it is common to many programming languages.

## For the Queen !

### Arithmetic
Let's introduce how to manipulate arithmetic, let's try the command:
```shell
a=42
echo $a+1
```

But it didn't add 1 to the variable a, did it ?
Indeed, in shell **everything is character (or sequence of characters)**
Thus, *$a+1* is the string "42+1" since $a refers to the content of the variable a which is also a string.

To use arithmetic we need to specify that a variable is a number. Execute the following commands:
```shell
let a=42
echo $a+1
```

But, but... Yes $a refers to the number 42, but "+1" is still a string.
So, for that we need to write and execute the following commands:
```shell
let a=42
let a=$a+1
echo $a
```

**Every time you want to assign an arithmetic variable you need to specify the keyword LET** otherwise it will be interpert as a string.

### A countdown

Let's do a countdown, copy-paste the command in the script screen and execute it:
```shell
#!/bash
# For with an already known list
for i in 10 9 8 7 6 5 4 3 2 1 0; do
    echo $i
done
echo "Cheese"
```

Let's iterate over a list:
```shell
#!/bash
# Move back in the tree to have more than one file
cd ..
# For using backquote (result of another command)
for file in `ls -a`; do
    echo $file
done
# Move back in our session - do not mind about the command
cd `ls -l | grep "^d" | cut -d' ' -f10`
```

A last example of loop. Try to guess what the code does:
```shell
#!/bash
let i=0
for j in `date`; do
    let i=$i+1
    echo $j
done
echo $i
```

## Meanwhile

### The Final Countdown
Let's do another countdown:
```shell
#!/bash
let i=10
while [[ $i != 0 ]]; do
	echo $i
	let i=$i-1
done
echo $i
```

#### NB

The double brackets are mandatory in **BASH** but this syntax may change in csh, zsh, ...

### Script specific variables

I did not introduce those specific variables of a script, try to guess what they are.
First, copy-paste the following script - you need to execute it at the moment (to save the script):
```shell
#!/bash
# Simple quote to make sure that variables won't be interpreted
a='$#'
b='$*'
c='$0'
d='$2' 
# Display the value of the variables
echo "First specific variable is $a: $#"
echo "Second specific variable is $b: $*"
echo "Other specific variable is $c: $0"
echo "Other specific variable is $d: $2"
```

Execute the following command in **the command-line** (do not touch to the previous script which is on the left screen):
```shell
bash main.sh
bash main.sh Hello
bash main.sh Hello World !
```

#### NB

$# : Number of argument including the script file's name
$* : All the arguments
$n : The nth argument, where n is a number.


## If, then, else FI

The if-statment is as follows:
```shell
#!/bash

if [[ $# != 0 ]]; then
    echo $#
else
    echo "Mmh"
fi
```

Let's try our script:
```shell
bash main.sh
bash main.sh 1
```

### Comparison

If you need to compare you may need to know how to compare. Here are the usual and common comparisons:

#### String comparison in BASH

1. Equal : string1 = string2
2. Not equal : string1 != string2

#### Integer Comparison in BASH

1. Equal (==) : int1 -eq int2
2. Not equal (!=) : int1 -ne int2
3. Less than (<) : int1 -lt int2
4. Less than or equal (<=) : int1 -le int2
5. Greater than (>) : int1 -gt int2
6. Greater than or equal (>=): int1 -ge int2

### Multiple Comparison

1. And : -a
2. Or : -o

```shell
if [[ $# -eq 2 -o $# -eq 5 ]]; then echo "Hello World!"; fi;
```

## Your turn !

### Exercise 1

Write a script! 

It returns *Hello World!* if and only if the number of argument is 2.
You have to tell the user if she/he has put too much or not enough arguments! In that case, you prompt the number of argument that have been passed and how many are expected.

<details><summary>Here's **a** solution</summary>

```shell
#!/bash

# First case, not enough arguments
if [ $# -le 1 ]; then
	echo "Missing one argument ! (Found $# : Required 2)"
    # Exit is equal to a return-statement is C/Java/...
	exit 0
fi;
# Second case, too much arguments
if [ $# -gt 2 ]; then
	echo "Too many inputs ! (Found $# : Required 2)"
	exit 0
fi;
# Last case, right number of arguments
if [ $# -eq 2 ]; then
    echo "Hello World!"
    exit 1
fi;
```

</details>

### Exercise 2

Write a script!

Count (display on the output) the number of file there are in the Home Directory, including hidden files.

<details><summary>Hint #1</summary>

The home directory is stocked in the variable:
```shell
$HOME
```

</details>

<details><summary>Hint #2</summary>

To display all files including hidden files, you need to use the command:
```shell
ls -a
```

</details>


<details><summary>Here's **a** solution</summary>

```shell
#!/bash

let i=0
for file in `ls -a $HOME`; do
	let i=$i+1
done;
echo "There are $i files in $HOME";
```

</details>

### Exercise 3

Write a script!

Ask again and again the question: *Answer by writing Yes or No* until the user writes exactly *Yes* or *No*.
You need the command *read* which allows to keep in a variable the user's input:
```shell
# Save user's input in the variable answer
read answer
```

<details><summary>Here's **a** solution</summary>

```shell
#!/bash

q="Write Yes or No (case sensitive!)"
echo $q
read answer
while [ $answer != "Yes" -a $answer != "No" ]; do
	echo $q
	read answer
done;
```

</details>

