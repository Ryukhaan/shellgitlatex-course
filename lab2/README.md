# Introduction

### **OPEN YOUR BROWSER AND GO TO THE URL**

### **<https://gitlab.com/Ryukhaan/shellgitlatex-course>**

## Goals

Welcome to your **second** lab day for Shell !

This time let's focus on redirection (pipes), conditionnal, loop and script.
It's also time to introduce you to git (at least web version on gitlab).

In Shell (and in general in computing) there can be several solutions to a problem !

As always, have fun, and enjoy the (remainder of the) class period !

### Connection
Connect to the website <https://replit.com/languages/bash>

## First Exercise - What's is a script

Let's move into the folder **Script** to learn more about script in shell.

## Second Exercise - Loop and Conditionnal

Ok! Now move into the folder **Loop** to discover how to make loop and conditionnal in shell.

## You have already finished ?

Write a script!

The script is as follows:
main.sh *name* *list_of_names*

It displays on the output the *list_of_names* without elements that are equal to *name*

Example:
```shell
bash main.sh dog cats dog rabbit cats horse dog crocodile cats
# Should return: cats rabbit cats horse crocodile cats
```
