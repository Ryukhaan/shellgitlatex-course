# Introduction

### **OPEN YOUR BROWSER AND GO TO THE URL**

### **<https://gitlab.com/Ryukhaan/shellgitlatex-course>**

## Goals

Welcome to your first lab day for Shell !

The primary goal of this lab is to ensure that your Shell, Git and Latex initiation went smoothly.

The following problems are not intended to be algorithmically challenging - just ways to use common commands of Shell and Git. Even if the problems seem simple, work through them quickly, and then you're free to go.

In Shell (and in general in computing) there can be several solutions to a problem !

As always, have fun, and enjoy the (remainder of the) class period !

## First Exercise

This exercise is here to show you some examples of the introduced command.

You just have to follow the instructions.

Type the command (or copy-paste it) into the terminal then press **ENTER** to execute it

### Connection
Connect to the website <https://replit.com/languages/bash>
        
### PWD (Print Working Directory)
Enter the command:
```shell
pwd
```
What happened ?

### ECHO
Enter the command: 
```shell
echo "Hello World !"
```
What did the command do ?

Enter the command:
```shell
echo $HOME
echo ~
```
What is the difference ?


### LS (LiSt)
Enter the command:
```shell
ls
```
How many file(s) are there ?
And now try this:
```shell
ls -a
```
How many file(s) are there now ? 

Let's try an other list command:
```shell
ls $HOME
```

What did the command do ?

### CD (Change Directory)
Enter following commands :
```shell
cd ..
pwd
ls
```
What is the difference with the previous command ?


Move back to your session. For that we need to go back into your session direction.

Display the contents of the **Home Directory**
```shell
ls
```

Your session folder is the directory with a name similar to a code. Then enter the command:
```shell
cd <your_session_number> 
```
(do not mind < and > )

#### Note

In order to be faster, you may use the key **TAB** (on your keyboard) in order to auto-complete what your writing.
Let's try it !
First let's back to the Home Directory:
```shell
cd ..
```
Or
```shell
cd $HOME
```

Start writing the command:
```shell
cd <the first two letters/numbers of your session>
```
And now press **TAB**

Magic ! And press **ENTER** to execute the command
And we're back your session's directory.

### MKDIR (MaKe DIRectory) and TOUCH
Enter the following commands:
```shell
mkdir first_folder
touch first_file.txt
ls
```
What happened ?

### CP (CoPy)
Try these command:
```shell
cp first_file.txt fist_folder/
ls my_first_folder/
```
What did the command **CP** do ?

### MV (MoVe)
Enter the commands:
```shell
mv my_first_file.txt shortfile.txt
ls
```
What did the command do ?

Now, try this:
```shell
mv my_first_folder/my_first_file.txt .
ls *.txt
```
What happened ? 

#### NOTE
With **ls** your can filter the prompted list.

Let's try to explain the syntax **\*.txt**

**\*** is the *joker* meaning the it can be whatever your want.

Then it is followed by **.txt**

So, it will list whatever you want followed by **.txt**. In other words, it will only prompt all files with the extension **.txt**

### ECHO and CAT (conCATenate)
Enter the following commands:
```shell
echo "Hello World !" > file.txt
cat file.txt
```
What did you see ?

Let's try another command:
```shell
ls -a > another.txt
cat another.txt
```
What happened ?

#### NOTE 
The symbol **>** is here to redirect the output of the command *ls -a* (which normally prompts the result on the terminal) into the file *file.txt* (or *another.txt*)

We will learn more about redirection in the next course.

For the moment just remember that with **>** you can write the output of a command to a file.


### TAIL and HEAD
Enter the following commands:
```shell
tail -n 1 another.txt 
tail -n 2 another.txt
head -n 1 another.txt 
head -n 2 another.txt
```

By default (meaning without the argument -n "x") these commands display the first ten (or the last ten) lines of a file.

## Exercise 1 - Your turn !


Let's try to do this.

First, you may refresh the website page (in order to have a cleaned session).

1. Create a temporary directory named *CFILE*

2. Move inside the directory and create a file named *LIST.TXT*

3. Create few files inside the Session Directory (the folder with the hashcode) with the extension ".c" and ".o" (named them as you want)

4. List all the files with the extension ".c" and ".o" of the Session Directory. 
For that you need to specify the absolute path, i.e. $HOME/your_session_id/what_you_looking_for

5. Write all the files with the extension ".c" and ".o" into the file *LIST.txt* 

6. Display the contents of LIST.txt

7. Copy one file ending with ".c" inside the Session Directory into the CFILE directory.

8. Change the file's extension to ".cplusplus".

9. Print the first **2** lines of LIST.txt. Then print the first **3** lines of LIST.txt

## Already finished ? Let's have fun

Let's practise a little more.

Connect to: <http://web.mit.edu/mprat/Public/web/Terminus/Web/main.html>
